package com.kryz.websocket.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ChatMessage {
    private MessageType type;
    private String content;
    private String sender;
}

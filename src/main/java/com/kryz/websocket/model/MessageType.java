package com.kryz.websocket.model;

public enum MessageType {
    CHAT,
    JOIN,
    LEAVE
}
